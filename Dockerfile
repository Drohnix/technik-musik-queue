FROM python:3.11-alpine

# Install gcc and other necessary build tools
RUN apk add --no-cache gcc musl-dev mariadb-connector-c-dev

EXPOSE 5000
RUN mkdir /app
WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY . /app

CMD python -m flask run --host=0.0.0.0