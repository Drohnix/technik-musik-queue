import spotipy.oauth2
from flask import render_template, url_for, request, redirect, flash
from flask_login import login_user, current_user, logout_user, login_required

from app import app, db
from cache import DbCacheHandler
from forms import LoginForm
from models import User, Party, OAuthToken, PartyConfig


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.args.get('party_key'):
        return redirect(url_for('party_view', party_key=request.args.get('party_key')))

    return render_template('index.html', title='Musikwünsche')


@app.route('/party/<party_key>', methods=['GET', 'POST'])
def party_view(party_key):
    party = Party.query.filter_by(party_key=party_key).first()

    if party is None:
        flash('Party konnte nicht gefunden werden')
        return redirect(url_for('index'))

    auth_manager = get_auth_manager(party.owner)

    if not auth_manager.validate_token(auth_manager.get_cached_token()):
        flash('Party wurde noch nicht gestartet, bitte versuche es später erneut')
        return redirect(url_for('index'))

    spotify = spotipy.Spotify(auth_manager=auth_manager)

    current = spotify.currently_playing()
    if current is None:
        flash('Party wurde noch nicht gestartet, bitte versuche es später erneut')
        return redirect(url_for('index'))

    current_track = current['item']
    if 'search' in request.args.keys():
        formatted_results = list(map(get_formatted_track,
                                     spotify.search(q=request.args['search'], limit=10)['tracks']['items']))
        return render_template('party.html',
                               title="Musikwünsche",
                               party=party,
                               current_track=get_formatted_track(current_track),
                               results=formatted_results,
                               search=request.args['search'])
    elif 'id' in request.form.keys():
        spotify.add_to_queue(request.form['id'])
        return redirect(url_for('party_view', party_key=party_key) + '?added')
    elif 'added' in request.args.keys():
        return render_template('party.html',
                               title="Musikwünsche",
                               party=party,
                               current_track=get_formatted_track(current_track),
                               added=True)

    return render_template('party.html', title="Musikwünsche", party=party,
                           current_track=get_formatted_track(current_track))


@app.route('/user')
@login_required
def user_view():
    auth_manager = get_auth_manager(current_user)

    if auth_manager.cache_handler.get_cached_token() is None:
        auth_uri = get_auth_manager(current_user).get_authorize_url()
        return render_template('user.html', title='Benutzer', auth_uri=auth_uri)

    parties = Party.query.filter_by(owner=User.query.filter_by(username=current_user.username).first())

    current = spotipy.Spotify(auth_manager=auth_manager).currently_playing()
    if current is not None:
        return render_template('user.html', title="Benutzer", parties=parties, current_track=get_formatted_track(current['item']))

    return render_template('user.html', title='Benutzer', parties=parties)


@app.route('/authorize')
@login_required
def authorize():
    if request.args.get("code"):
        get_auth_manager(current_user).get_access_token(request.args.get("code"))
        return redirect(url_for('user_view'))

    return redirect(url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))

        login_user(user, form.remember_me.data)
        next_uri = request.args.get('next')
        return redirect(next_uri or url_for('index'))

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    return render_template('login.html', title="Login", form=form)


@app.route('/logout', methods=['POST'])
def logout():
    logout_user()
    return redirect(url_for('index'))


# ToDo: Run at startup, first check whether the database exists, if not create it
@app.route('/init')
def init_db():
    if db.inspect(db.engine).has_table('user'):
        return "Database already initialized"

    db.create_all()

    username = app.config.get('ADMIN_USER')
    user = User(username=username)
    password = app.config.get('ADMIN_PASSWORD')
    user.set_password(password)
    db.session.add(user)

    party_config = PartyConfig(allow_voting=False, allow_moving=False, enqueue_cooldown=60)
    db.session.add(party_config)

    party = Party(party_config, user)
    db.session.add(party)

    db.session.commit()
    return "Database initialized\n Username: {}\n Password: {}".format(username, password)


def get_auth_manager(owner):
    cache_handler = DbCacheHandler(owner=owner)
    return spotipy.oauth2.SpotifyOAuth(scope=app.config.get('SPOTIPY_PERMISSION_SCOPE'),
                                       cache_handler=cache_handler, show_dialog=True)


def get_formatted_track(track):
    return {
        'id': track['id'],
        'name': track['name'],
        'artist': track['artists'][0]['name'],
        'coverart': track['album']['images'][0]['url']
    }
