import random
import string
from datetime import datetime

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db
from app import login


def id_generator(length=6, characters=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(characters) for _ in range(length))


@login.user_loader
def load_user(identifier):
    return User.query.get(int(identifier))


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)

    username = db.Column(db.String(32), index=True, unique=True)
    password = db.Column(db.String(128))
    oauth_token = db.relationship('OAuthToken', backref='user', uselist=False)
    parties = db.relationship('Party')

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def __init__(self, username):
        self.username = username


class Party(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    owner = db.relationship('User', back_populates='parties')
    party_key = db.Column(db.String(6), unique=True, default=id_generator, index=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    config = db.relationship('PartyConfig', backref='party', uselist=False)

    def __init__(self, config, owner):
        self.config = config
        self.owner = owner


class PartyConfig(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    party_id = db.Column(db.Integer, db.ForeignKey('party.id'))

    allow_voting = db.Column(db.Boolean)
    allow_moving = db.Column(db.Boolean)
    enqueue_cooldown = db.Column(db.Integer)

    def __init__(self, allow_voting, allow_moving, enqueue_cooldown):
        self.allow_voting = allow_voting
        self.allow_moving = allow_moving
        self.enqueue_cooldown = enqueue_cooldown


class OAuthToken(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    access_token = db.Column(db.String(250))
    token_type = db.Column(db.String(32))
    expires_in = db.Column(db.Integer)
    expires_at = db.Column(db.Integer)
    refresh_token = db.Column(db.String(250))

    def __init__(self, access_token, token_type, expires_in, expires_at, refresh_token):
        self.access_token = access_token
        self.token_type = token_type
        self.expires_in = expires_in
        self.expires_at = expires_at
        self.refresh_token = refresh_token
