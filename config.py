import os
from secrets import token_hex


class Config(object):
    # Flask
    SECRET_KEY = token_hex(16)

    # Login
    # only used on first run, every new database will have this user
    ADMIN_USER = os.environ.get('ADMIN_USER', 'admin')
    ADMIN_PASSWORD = os.environ.get('ADMIN_PASSWORD', token_hex(16))

    # Database
    SQLALCHEMY_DATABASE_URI = "mariadb+mariadbconnector://{}:{}@{}:{}/{}".format(
        os.environ.get('DATABASE_USER', 'spotipy'),
        os.environ.get('DATABASE_PASSWORD', 'spotipy'),
        os.environ.get('DATABASE_SERVER', '127.0.0.1'),
        os.environ.get('DATABASE_PORT', '3307'),
        os.environ.get('DATABASE_PATH', 'spotipy'))
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Spotipy
    SPOTIPY_CLIENT_ID = os.environ.get('SPOTIPY_CLIENT_ID', 'client-id')
    SPOTIPY_CLIENT_SECRET = os.environ.get('SPOTIPY_CLIENT_SECRET', 'client-secret')
    SPOTIPY_REDIRECT_URI = os.environ.get('SPOTIPY_REDIRECT_URI', 'http://127.0.0.1:5000/authorize')
    SPOTIPY_PERMISSION_SCOPE = os.environ.get('SPOTIPY_PERMISSION_SCOPE', "user-library-read app-remote-control user-read-currently-playing streaming user-modify-playback-state user-read-playback-position user-read-playback-state")
