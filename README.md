# Setup

1. create .env file
```environment
ADMIN_USER=user
ADMIN_PASSWORD=password

SPOTIPY_CLIENT_ID=client-id
SPOTIPY_CLIENT_SECRET=client-secret
SPOTIPY_REDIRECT_URI=http://127.0.0.1:5000/authorize
SPOTIPY_PERMISSION_SCOPE=user-library-read app-remote-control user-read-currently-playing streaming user-modify-playback-state user-read-playback-position user-read-playback-state
```
2. run compose.yml
```bash
docker-compose up -d --build
```
3. init database by opening the browser and go to `/init`
4. Login as initial user with the credentials in the .env file
5. Click on the username
6. Click on 'Verbinden' and follow the instructions to link spotify
7. Login again and click on username
8. Click on the created party