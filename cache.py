from spotipy import CacheHandler

from app import app, db
from models import OAuthToken


class DbCacheHandler(CacheHandler):

    def __init__(self, owner):
        self.owner = owner

    def get_cached_token(self):
        token = self.owner.oauth_token

        if token is None:
            return None

        return {"access_token": token.access_token, "token_type": token.token_type, "expires_in": token.expires_in,
                "scope": app.config.get('SPOTIPY_PERMISSION_SCOPE'), "expires_at": token.expires_at,
                "refresh_token": token.refresh_token}

    def save_token_to_cache(self, token_info):
        existing_token = self.owner.oauth_token

        token = OAuthToken(access_token=token_info.get('access_token'), token_type=token_info.get('token_type'),
                           expires_in=token_info.get('expires_in'), expires_at=token_info.get('expires_at'),
                           refresh_token=token_info.get('refresh_token'))
        db.session.add(token)
        self.owner.oauth_token = token

        if existing_token is not None:
            db.session.delete(existing_token)

        db.session.commit()
        return None
